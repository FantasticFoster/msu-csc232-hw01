/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Bag.h
 * @authors Frank M. Carrano
 *          Timothy M. Henry
 * @brief  Bag interface specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef HW01_BAG_H
#define HW01_BAG_H

#include <vector>

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Bag Interface introduced in Chapter 1 in Listing 1-1 with
     * modifications by Jim Daehn.
     * @tparam T class template parameter; the type of element stored in this
     * `Bag`.
     */
    template <typename T>
    class Bag {
    public:
        /**
         * @brief Gets the current number of entries in this bag.
         * @return The integer number of entries currently in this `Bag` is
         * returned.
         */
        virtual int getCurrentSize() const = 0;

        /**
         * @brief Determine whether this `Bag` is empty.
         * @return  True if this `Bag` is empty; false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * @brief Adds a new entry to this `Bag`.
         * @post If successful, `newEntry` is stored in the bag and the count
         * of items in the bag has increased by 1.
         * @param newEntry The object to be added as a new entry.
         * @return True if addition was successful, or false if not
         */
        virtual bool add(const T& newEntry) = 0;

        /**
         * @brief Removes one occurrence of a given entry from this `Bag` if
         * possible.
         * @post If successful, `anEntry` has been removed from the bag and
         * the count of items i the bag has decreased by 1.
         * @param anEntry The entry to be removed.
         * @return True if removal was successful, or false if not.
         */
        virtual bool remove(const T& anEntry) = 0;

        /**
         * @brief Removes all entries from this bag.
         * @post Bag contains no items, and the count of items is 0.
         */
        virtual void clear() = 0;

        /**
         * @brief Counts the number of times a given entry appears in this bag.
         * @param anEntry The entry to be counted.
         * @return The number of times `anEntry` appears in the bag.
         */
        virtual int getFrequencyOf(const T& anEntry) const = 0;

        /**
         * @brief Tests whether this bag contains a given entry.
         * @param anEntry The entry to locate.
         * @return True if bag contains `anEntry`, or false otherwise.
         */
        virtual bool contains(const T& anEntry) const = 0;

        /**
         * @brief Empties and then fills a given `std::vector` with all
         * entries that are in this `Bag`.
         * @return A `std::vector` containing copies of all the entries in
         * this `Bag`.
         */
        virtual std::vector<T> toVector() const = 0;
		
		// TODO: Exercise 6 - provide union specification below with appropriate
        // doxygen comments appropriate comments that fully specify the
        // behavior of this method. When you have completed this step, commit
        // your changes with the following commit message:
        // CSC232-HW01 - Completed Exercise 6.
		
		/**
         * @brief A new bag is created contains the contents of two bags (union).
         * @post If successful, a 'bag3' is created containing all entries
		 * from 'bag1' and 'bag2'.
         * @param bag2 A Bag to be added to the bag1 vector.
         * @return A Bag containing copies of the entries from both 
		 * bags.
         */
		 virtual Bag unionBag(Bag bag2) = 0;

        // TODO: Exercise 7 - provide intersection specification below with
        // appropriate doxygen comments that fully specify the behavior of this
        // method. When you have completed this step, commit your changes with
        // the following commit message:
        // CSC232-HW01 - Completed Exercise 7.
		
		/**
		* @brief  A new bag is created containing the entries occuring in BOTH of two bags 
		* (intersection).
		* @post If successful, a 'bag3' is created containing entries occuring in BOTH 'bag1'
		* and 'bag2'.
		* @param bag2 A Bag to be compared to the first bag in order to create 'bag3'.
		* @return A Bag containing copies of the entries that appear in BOTH a 'bag1'
		* and a 'bag2'.
		*/
		virtual Bag intersectionBag(Bag bag2) = 0;

        // TODO: Exercise 8 - provide difference specification below with
        // appropriate doxygen comments that fully specify the behavior of this
        // method. When you have completed this step, commit your changes with
        // the following commit message:
        // CSC232-HW01 - Completed Exercise 8.
		
		/**
		* @brief A new bag is created containing the contents of the second bag subtracted from
		* the first bag (difference).
		* @post If successful, a 'bag3' is created containing entries occuring in BOTH 'bag1' and
		* 'bag2'.
		* @param bag2 A std::vector to be subtracted from the first bag in order to create 'bag3'.
		* @return A Bag containing the difference of 'bag1' and 'bag2'.
		*/
		virtual Bag differenceBag(Bag bag2) = 0;

        // TODO: Programming Problem 6 - provide inline definitions of the
        // above operations independently of the bag's implementation by
        // using only ADT bag operations. When you have completed this step,
        // commit your changes with the following commit message:
        // CSC232-HW01 - Completed Programming Problem 6.
		Bag unionBag(Bag bag2)
		{
			Bag bag1 = Bag::Vector();
			int bagSpot = 0;
			for (bag1[bagSpot]; bagSpot <= bag1.size(); bagSpot++)
			{
				Bag bag3.add(bag1[bagSpot]);
			}
			for (bag2[bagSpot]; bagSpot <= bag2.size(); bagSpot++)
			{
				bag3.add(bag2[bagSpot]);
			}
			return bag3;
		}
		
		Bag intersectionBag(Bag bag2)
		{
			Bag bag1 = Bag::toVector();
			int bagSpot = 0;
			for (bag1[bagSpot]; bagSpot <= bag1.size(); bagSpot++)
			{
				if (bag2.contains(bag1[bagSpot])
				{
					Bag bag3.add(bag1[bagSpot]);
				}
			}
			return bag3;
		}
		
		Bag differenceBag(Bag bag2)
		{
			Bag bag1 = Bag::toVector();
			int bagSpot = 0;
			Bag bag3 = bag1;
			Bag bag4 = bag2;
			for(bag4[bagSpot]; bagSpot <= bag1.size(); bagSpot++)
			{
				if (bag3.contains(bag4[bagSpot])
				{
					bag4.remove(bag4[bagSpot]);
					bag3.remove(bag4[bagSpot]);
				}
				else
					bag4.remove(bag4[bagSpot]);
			}
			return bag3;
		}
        /**
         * @brief Destroys this bag and frees its assigned memory.
         */
        virtual ~Bag() {
            // inlined, no-op
        }
    }; // end Bag
} // end namespace csc232

#endif //HW01_BAG_H
