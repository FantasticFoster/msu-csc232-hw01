# Codeblocks
This directory should be your target for Codeblocks project files. To generate Codeblocks project files, type

```
$ cmake -G "CodeBlocks - Unix Makefiles" ../..
```

at the command line prompt.
