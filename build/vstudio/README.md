# Visual Studio
This directory should be your target for Visual Studio project files. To generate Visual Studio project files, type

```
$ cmake -G " Visual Studio [version] [year] [arch]" ../..
```

at the command line prompt. Note the place holders:

* Visual Studio 15 2017 [arch] = Generates Visual Studio 2017 project files. Optional [arch] can be "Win64" or "ARM".
* Visual Studio 14 2015 [arch] = Generates Visual Studio 2015 project files. Optional [arch] can be "Win64" or "ARM".
* Visual Studio 12 2013 [arch] = Generates Visual Studio 2013 project files. Optional [arch] can be "Win64" or "ARM".
* Visual Studio 11 2012 [arch] = Generates Visual Studio 2012 project files. Optional [arch] can be "Win64" or "ARM".
* Visual Studio 10 2010 [arch] = Generates Visual Studio 2010 project files. Optional [arch] can be "Win64" or "IA64".
* Visual Studio 9 2008 [arch]  = Generates Visual Studio 2008 project files. Optional [arch] can be "Win64" or "IA64".
* Visual Studio 8 2005 [arch]  = Generates Visual Studio 2005 project files. Optional [arch] can be "Win64".
* Visual Studio 7 .NET 2003    = Deprecated.  Generates Visual Studio .NET 2003 project files.
